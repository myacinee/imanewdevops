const request = require('supertest')
const { expect } = require('chai')
const app = require('../../app.js')

function connectToExpress () {
  const agent = request.agent(app)
  return agent
}

describe('GET /', () => {

  beforeEach(() => {
    // pré-requête pour initialiser l'environnement
  })

  it("should display a 'Hello world'", async () => {
    const agent = connectToExpress()

    return agent.get('/')
      .expect(200)
      .then((res) => {
        // console.log('res', res.body)
        expect(res.body).to.be("Hello world");
      })
  })
})
