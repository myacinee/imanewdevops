FROM postgres:alpine

RUN docker run --name some-postgres -e POSTGRES_PASSWORD=mysecretpassword -d postgres

EXPOSE 80